(function($) {
    'use strict';

     /**
      * Фиксация меню при скролле и плавное его уменьшение
      */
     $(document).on("scroll",function(){
        var $header = $(".header");
        var $logo = $(".header-logo svg");

        if ($(window).width() >= 768) {
           if ($(document).scrollTop() > 0){
               $header.addClass("fixed-menu");
               $header.stop().animate({height: "70px"}, 1000)
               $logo.stop().animate({width: "66px", height: "53px"}, 1000)
           } else {
               $header.removeClass("fixed-menu");
               $header.stop().animate({height: "160px"}, 1000)
               $logo.stop().animate({width: "150px", height: "120px"}, 1000)
           }
       } else {
           if ($(document).scrollTop() > 0){
               $header.addClass("fixed-menu");
               $header.stop()
               $logo.stop()
           } else {
               $header.removeClass("fixed-menu");
           }
       }
     });
})(jQuery);
