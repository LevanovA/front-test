'use strict';

(function () {
    var select = document.querySelector('.tabs-select');

    select.onchange = function () {
        document.querySelector('.js-tab-content.active').classList.remove('active');
        document.querySelector('.js-tab-header.active').classList.remove('active');
        document.querySelectorAll('.js-tab-content')[select.options.selectedIndex].classList.add('active');
        document.querySelectorAll('.js-tab-header')[select.options.selectedIndex].classList.add('active');
    }
})();
