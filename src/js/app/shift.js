(function($) {
    'use strict';

     /**
      * Перемещение изображений внутри блока
      */

     /**
      * Перемещение изображений по клику
      */
     $(document).ready(function () {
        var boxPanda = $('.img-box--panda');
        var boxCat = $('.img-box--cat');
        var imgPanda = $('.img-box--panda img');
        var imgCat = $('.img-box--cat img');
        var main = $('.main')

        imgCat.on('mousedown', function (evt) {
            evt.preventDefault()
            boxCat.on('mousemove', function (event) {
                var pos = $(this).offset();
                var elem_left = pos.left.toFixed(0);
                var elem_top = pos.top.toFixed(0);
                var x = event.pageX - elem_left;
                var y = event.pageY - elem_top;
                imgCat.offset({top: imgCat.css({'top': (y - imgCat.height()/2)}), left: imgCat.css({'left': (x - imgCat.width()/2)})})

                main.on('mouseup', function () {
                    boxCat.off('mousemove');
                });
            });
        });

     /**
      * Перемещение изображений по по наведению
      */
        boxPanda.on('mousemove', function (event) {
            var pos = $(this).offset();
            var elem_left = pos.left.toFixed(0);
            var elem_top = pos.top.toFixed(0);
            var x = event.pageX - elem_left;
            var y = event.pageY - elem_top;
            imgPanda.offset({top: imgPanda.css({'top': (y - imgPanda.height()/2)}), left: imgPanda.css({'left': (x - imgPanda.width()/2)})})
        });
    });
})(jQuery);
